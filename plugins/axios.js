export default function ({ $axios, redirect, store }) {
  $axios.onRequest((config) => {
    if (store.getters['auth/isAuthenticated'] && !config.headers.common.Authorization) {
      const token = store.getters['auth/token']
      config.headers.common.Authorization = `Bearer ${token}`
    }
    return config
  })

  $axios.onError((error) => {
    if (error.response) {
      if (error.response.status === 401) {
        redirect('/admin/login?message=session')
        store.dispath('auth/logout')
      }
      if (error.response.status === 500) {
        console.error('Server 500 error')
      }
    }
  })
}
