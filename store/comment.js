export const state = () => ({
  error: null
})

export const mutations = {
  setError (state, error) {
    state.error = error
  },
  clearError (state) {
    state.error = null
  }
}

export const actions = {
  async create ({ commit }, data) {
    try {
      return await this.$axios.$post('/api/comment', data)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  }
}

export const getters = {
  isError: state => state.error
}
