
module.exports = {
  mode: 'universal',
  target: 'server',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  loading: { color: '#409EFF' },

  css: [
    '@/theme/index.scss',
    'element-ui/lib/theme-chalk/index.css'
  ],
  plugins: [
    '@/plugins/globals',
    '@/plugins/axios'
  ],
  components: true,
  buildModules: [
    '@nuxtjs/eslint-module'
  ],

  telemetry: false,

  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/axios'
  ],

  axios: {
    baseURL: process.env.BASE_URL || 'http://localhost:3000'
  },

  env: {
    appName: 'SSR bLog'
  },

  build: {
    transpile: [/^element-ui/],
    extend (config, ctx) {

    }
  }
}
